from utils import *
from train import *
from predict import *
import numpy as np
import pickle
import tensorflow as tf

gpu = 0
os.environ["CUDA_VISIBLE_DEVICES"] = str(gpu)
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.Session(config=config)

################ PARAMETERS  #####################

nbClasses = 11  # Number of different pollen types in the data-set #11
nTrain = 9 * nbClasses  # Number of training images 9
nTrainMixed = 60              #60
nVal = 3 * nbClasses  # Number of validation images 3
nTest = 33  # Number of test images 33
nbEpoch = 100   #100

#####################################
# Segmenters
#####################################

def train_segmenter():
    parameters = {'nTrain': nTrain+nTrainMixed,
                  'nVal': nVal,
                  'nTest': nTest,
                  'nbClasses': nbClasses,
                  'model': 'unet_2d',
                  'n_layers': 5,
                  'n_feat_maps': 16,
                  'batch_size': 2,
                  'nb_epoch': nbEpoch,
                  'lr': 2e-4,  # Initial learning rate      2e-4 for All cells
                  'decay_factor': 0.5,  # Factor by which the learning rate is reduced every step
                  'step_size': 20,  # Number of epochs after which the LR is reduced by the decay factor
                  'loss': 'categorCross',
                  'en_online': 0,
                  'wd': 0,
                  'dropout': 0,
                  'bn': 0,
                  'factor_augment': 1,
                  'init': 'he_uniform',
                  'modality': 'nuclei'}
    parameters_entries = ('model', 'n_layers', 'modality')
    input_images = np.load('./samples/base.npy')
    output_masks = np.load('./samples/labels.npy')
    run_cv_train(parameters, input_images, output_masks, parameters_entries)


def predict_segmenter():
    input_images = np.load('./samples/base.npy')
    labels = np.load('./samples/labels.npy')
    predict_set = 'test'
    results_name_short = 'model_unet_2d_n_layers_5_modality_nuclei'
    run_cv_predict(input_images, predict_set, results_name_short)
    concatenate_predictions(predict_set, results_name_short, labels.shape)


#####################################
# Utils
#####################################

def run_cv_train(params, images, masks, params_entries):
    # Save parameters and create results folder path
    results_name_short = params2name({k: params[k] for k in params_entries})
    results_path = './results/' + results_name_short
    save_params(params, results_path)
    params['nTrain'] = params['nTrain'] * params['factor_augment']

    # Run the cross validation training
    for i in [0, 1]:
        print('======================================== cvNum ' + str(i) + ' ========================================')
        cv = cv_index_generator(params, params['nTrain'], results_path, i, True)
        train_model(params, cv, images, masks, 0, gpu, results_path, True)


def run_cv_predict(images, predict_set, results_name_short):
    # Load params and create results folder path
    # global selected_indices
    results_path = './results/' + results_name_short
    with open(results_path + '/params.p', 'rb') as handle:
        params = pickle.load(handle)
    nbr_classes = nbClasses + 1  # including the background

    # Run the cross validation prediction
    for i in [0, 1]:
        print('======================================== cvNum ' + str(i) + ' ========================================')
        cv = cv_index_generator(params, params['nTrain'], results_path, i, True)
        if predict_set == 'val':
            selected_indices = cv['val']
        elif predict_set == 'test':
            selected_indices = cv['test']
        predict_model(images[selected_indices], params, nbr_classes, cv, results_path, True)


def concatenate_predictions(predict_set, results_name_short, shape_images):
    # Load params
    results_path = './results/' + results_name_short
    with open(results_path + '/params.p', 'rb') as handle:
        params = pickle.load(handle)

    # Run the cross validation prediction
    if predict_set == 'val':
        predictions = np.zeros((shape_images[0], shape_images[1], shape_images[2], shape_images[3]))
    elif predict_set == 'test':
        predictions = np.zeros((shape_images[0], shape_images[1], shape_images[2], shape_images[3]))
        result = np.zeros((shape_images[0], shape_images[1], shape_images[2], shape_images[3]),dtype='uint8')
        predGen = np.zeros((shape_images[0], nbClasses + 1))

    for i in [0, 1]:
        print('======================================== cvNum ' + str(i) + ' ========================================')
        cv = cv_index_generator(params, params['nTrain'], results_path, i, True)

        predictions_fold = np.load(results_path + '/firstval' + str(cv['val'][0]) + '/prediction.npy')
        result_fold = np.load(results_path + '/firstval' + str(cv['val'][0]) + '/result.npy')
        predGen_fold = np.load(results_path + '/firstval' + str(cv['val'][0]) + '/predGen.npy')
        if predict_set == 'val':
            predictions[cv['val']] = predictions_fold
        elif predict_set == 'test':
            predictions[cv['test']] = predictions_fold
            predGen[cv['test']] = predGen_fold
            result[cv['test']] = result_fold
            # print(predictions.shape)

    predictions = predictions.astype(np.uint8)

    np.save(results_path + '/prediction.npy', predictions)
    np.save(results_path + '/predGen.npy', predGen)  # General prediction
    np.save(results_path + '/result.npy', result)


#####################################
# Run segmenters
#####################################
if __name__ == '__main__':
    train_segmenter()
    predict_segmenter()
