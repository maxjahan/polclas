import numpy as np
import time
from matplotlib import pyplot as plt
import os
from sklearn.metrics import *
from run import nTest
from utils import jaccard

project_path = os.path.dirname(os.path.dirname(__file__))  # directory TFE

# ----------- Load data arrays --------------
img_array1 = np.load('base.npy')
img_array2 = np.load('masks.npy')
labels = np.load('labels.npy')
truth = np.load('truth.npy')

#softmax = np.load(project_path + '/results/model_unet_2d_n_layers_5_modality_nuclei/prediction.npy')
predGen = np.load(project_path + '/results/model_unet_2d_n_layers_5_modality_nuclei/predGen.npy')
result = np.load(project_path + '/results/model_unet_2d_n_layers_5_modality_nuclei/result.npy')


print(img_array1.shape)
print(labels.shape)
print(result.shape)
print(predGen.shape)
print(truth.shape)

nIm = img_array1.shape[0]
Ctot= 11

jacScore =  np.empty((nTest, 11))
jacScore[:] = np.NaN
jacScoreAverage = np.zeros((11))

a = [1,2,3,4,5,6,7,8,9,10,11]
# a =[3,5,6,10]
Cl= Ctot - len(a)  #elligible classes
# nIm - nTest
for i in range(nIm - nTest, nIm):
    f, axarr = plt.subplots(2, 1+len(a))
    # f.suptitle('The % of Pollen background is : ' + str(round(100*predGen[i, 0],2)))
    axarr[0,0].imshow(np.uint8(img_array1[i]))
    axarr[0,0].title.set_text('Original')
    # axarr[0,1].imshow(img_array2[i, :, :, 0])
    # axarr[0,1].title.set_text('Full mask')
    axarr[0, 1].set_visible(True)

    # axarr[0,1].title.set_text('Background')
    axarr[1, 0].set_visible(False)
    # axarr[1, 1].set_visible(False)
    # axarr[1, 1].imshow(result[i, :, :, 0].astype(np.uint8))

    # BGtruth = result[i, :, :, 0].flatten()
    # BGpred  = labels[i, :, :, 0].flatten()
    BGtruth = result[i, :, :, 0]
    BGpred  = labels[i, :, :, 0]



    k = 0
    for j in a:

        axarr[0, k+1].imshow(labels[i, :, :, j])
        axarr[0, k+1].title.set_text('Pollen'+ str(j))
        axarr[0, k+1].axis('off')
        axarr[0, k+1].title.set_text(' C' + str(j) + ':'+ str(round(100 * truth[i, j], 2)) + '%')

        axarr[1, k+1].imshow(result[i, :, :, j])
        axarr[1, k+1].axis('off')
        axarr[1, k+1].title.set_text(str(round(100*predGen[i, j], 2))+ '%')
        k = k+1
        # BGtruth = result[i, :, :, j]
        # BGpred =  labels[i, :, :, j]
        BGtruth = labels[i, :, :, j].flatten()
        BGpred  = result[i, :, :, j].flatten()

        # if (j == 0): jacScore[i-(nIm - nTest),j] = round(jaccard_score(BGtruth, BGpred),2)
        jacScore[i - (nIm - nTest), j-1] = round(np.count_nonzero(BGtruth), 2)
        # elif BGtruth.any():
            # jacScore[i-(nIm - nTest),j] = round(jaccard_score(BGtruth, BGpred),2)


        # if k == len(a): print(jacScore)
    # accScoreSum = np.sum(jacScore, axis=0)
    jacScoreAverage = np.sum(jacScore,axis=0)
    jacScoreAverage =  100*np.divide(jacScoreAverage, np.sum(jacScoreAverage))
    print('acc score:' + str(jacScoreAverage))


    print(i)
    # plt.show()
    # time.sleep(0.001)
