from keras.models import Model
from keras.layers import Input, concatenate, Conv3D, MaxPooling3D, Conv3DTranspose, Conv2D, MaxPooling2D, \
    Conv2DTranspose, Dropout, Add, Lambda, multiply, SpatialDropout3D, SpatialDropout2D, LeakyReLU, BatchNormalization
import tensorflow as tf
from keras.callbacks import LearningRateScheduler
from keras import backend as K
import keras
import numpy as np
import math

from keras.regularizers import l2
from keras.optimizers import Adam
import pickle
import os
import matplotlib.pyplot as plt
import csv


###############################
# Losses
###############################


# "Twersky loss function for image segmentation using 3D FCDN"
# -> the score is computed for each class separately and then summed
# alpha=beta=0.5 : dice coefficient
# alpha=beta=1   : tanimoto coefficient (also known as jaccard)
# alpha+beta=1   : produces set of F*-scores
# implemented by E. Moebel, 06/04/18
def tversky_loss(y_true, y_pred):
    alpha = 0.5
    beta = 0.5

    ones = K.ones(K.shape(y_true))
    p0 = y_pred  # proba that voxels are class i
    p1 = ones - y_pred  # proba that voxels are not class i
    g0 = y_true
    g1 = ones - y_true

    num = K.sum(p0 * g0, (0, 1, 2,))
    den = num + alpha * K.sum(p0 * g1, (0, 1, 2,)) + beta * K.sum(p1 * g0, (0, 1, 2,))

    T = K.sum(num / den)  # when summing over classes, T has dynamic range [0 Ncl]

    Ncl = K.cast(K.shape(y_true)[-1], 'float32')
    return Ncl - T


def generalized_dice_coeff(y_true, y_pred):
    Ncl = y_pred.shape[-1]
    w = K.zeros(shape=(Ncl,))
    w = K.sum(y_true, axis=(0, 1, 2))
    w = 1 / (w ** 2 + 0.000001)
    # Compute gen dice coef:
    numerator = y_true * y_pred
    numerator = w * K.sum(numerator, (0, 1, 2, 3))
    numerator = K.sum(numerator)

    denominator = y_true + y_pred
    denominator = w * K.sum(denominator, (0, 1, 2, 3))
    denominator = K.sum(denominator)

    gen_dice_coef = 2 * numerator / denominator

    return gen_dice_coef


def generalized_dice_loss(y_true, y_pred):
    return 1 - generalized_dice_coeff(y_true, y_pred)


def dice_loss_2d(y_true, y_pred):
    smooth = 1
    sh = tf.shape(y_true)
    y_true_f = tf.transpose(tf.reshape(y_true, [sh[0], sh[1] * sh[2]]))
    y_pred_f = tf.transpose(tf.reshape(y_pred, [sh[0], sh[1] * sh[2]]))
    intersection = tf.multiply(y_true_f, y_pred_f)
    intersection = tf.reduce_sum(intersection, 0)
    card_y_true = tf.reduce_sum(y_true_f, 0)
    card_y_pred = tf.reduce_sum(y_pred_f, 0)
    dices = tf.truediv(2 * intersection, card_y_true + card_y_pred + smooth)
    return -tf.reduce_mean(dices)


###############################
# Metrics
###############################

def dice_2d(y_true, y_pred):
    sh = tf.shape(y_true)
    y_true_f = tf.transpose(tf.reshape(y_true, [sh[0], sh[1] * sh[2] * sh[3]]))
    y_pred_f = tf.transpose(tf.reshape(y_pred, [sh[0], sh[1] * sh[2] * sh[3]]))
    y_pred_f = K.cast(K.greater(y_pred_f, 0.5), K.floatx())
    intersection = tf.multiply(y_true_f, y_pred_f)
    intersection = tf.reduce_sum(intersection, 0)
    card_y_true = tf.reduce_sum(y_true_f, 0)
    card_y_pred = tf.reduce_sum(y_pred_f, 0)
    dices = tf.truediv(2 * intersection, card_y_true + card_y_pred)
    return 1-tf.reduce_mean(dices)



###############################
# Models
###############################

def unet_2d(params):
    nb_layers = params['n_layers']
    nb_features = params['n_feat_maps']

    # Input layer
    inputs = Input(batch_shape=(None, None, None, 3))
    # Encoding part
    skips = []
    x = inputs
    for i in range(nb_layers):
        # First conv, bn, ReLu, dropout block
        x = Conv2D(nb_features, (3, 3), activation='linear', padding='same',
                   kernel_initializer=params['init'], bias_initializer=params['init'],
                   kernel_regularizer=l2(params['wd']), bias_regularizer=l2(params['wd']), )(x)
        x = BatchNormalization()(x) if params['bn'] else x
        x = LeakyReLU(alpha=0.0)(x)
        x = SpatialDropout2D(params['dropout'])(x) if params['dropout'] != 0 else x

        # Second conv, bn, ReLu, dropout block
        x = Conv2D(nb_features, (3, 3), activation='linear', padding='same',
                   kernel_initializer=params['init'], bias_initializer=params['init'],
                   kernel_regularizer=l2(params['wd']), bias_regularizer=l2(params['wd']))(x)
        x = BatchNormalization()(x) if params['bn'] else x
        x = LeakyReLU(alpha=0.0)(x)
        x = SpatialDropout2D(params['dropout'])(x) if params['dropout'] != 0 else x

        # Skip connection and maxpooling
        skips.append(x)
        x = MaxPooling2D(pool_size=(2, 2))(x)
        nb_features = nb_features * 2

    # Bottleneck
    x = Conv2D(nb_features, (3, 3), activation='linear', padding='same',
               kernel_initializer=params['init'], bias_initializer=params['init'],
               kernel_regularizer=l2(params['wd']), bias_regularizer=l2(params['wd']))(x)
    x = BatchNormalization()(x) if params['bn'] else x
    x = LeakyReLU(alpha=0.0)(x)
    x = SpatialDropout2D(params['dropout'])(x) if params['dropout'] != 0 else x
    x = Conv2D(nb_features, (3, 3), activation='linear', padding='same',
               kernel_initializer=params['init'], bias_initializer=params['init'],
               kernel_regularizer=l2(params['wd']), bias_regularizer=l2(params['wd']))(x)
    x = BatchNormalization()(x) if params['bn'] else x
    x = LeakyReLU(alpha=0.0)(x)
    x = SpatialDropout2D(params['dropout'])(x) if params['dropout'] != 0 else x

    # Decoding part
    for i in reversed(range(nb_layers)):
        nb_features = int(nb_features / 2)

        # Upsampling and concatenate
        x = concatenate([Conv2DTranspose(nb_features, (2, 2), strides=(2, 2), padding='same',
                                         kernel_initializer=params['init'], bias_initializer=params['init'],
                                         kernel_regularizer=l2(params['wd']), bias_regularizer=l2(params['wd']))(x),
                         skips[i]], axis=3)

        # First conv, bn, ReLu, dropout block
        x = Conv2D(nb_features, (3, 3), activation='linear', padding='same', input_shape=(None, None, 3),
                   kernel_initializer=params['init'], bias_initializer=params['init'],
                   kernel_regularizer=l2(params['wd']), bias_regularizer=l2(params['wd']))(x)
        x = BatchNormalization()(x) if params['bn'] else x
        x = LeakyReLU(alpha=0.0)(x)
        x = SpatialDropout2D(params['dropout'])(x) if params['dropout'] != 0 else x

        # Second conv, bn, ReLu, dropout block
        x = Conv2D(nb_features, (3, 3), activation='linear', padding='same',
                   kernel_initializer=params['init'], bias_initializer=params['init'],
                   kernel_regularizer=l2(params['wd']), bias_regularizer=l2(params['wd']))(x)
        x = BatchNormalization()(x) if params['bn'] else x
        x = LeakyReLU(alpha=0.0)(x)
        x = SpatialDropout2D(params['dropout'])(x) if params['dropout'] != 0 else x

    # Output layer
    outputs = Conv2D(12, (1, 1), activation='softmax',   #activation='sigmoid'
                     kernel_initializer=params['init'], bias_initializer=params['init'])(x)

    model = Model(inputs=[inputs], outputs=[outputs])
    if params['loss'] == 'categorCross':
        model.compile(optimizer=Adam(params['lr']), loss='categorical_crossentropy', metrics=['categorical_crossentropy'])  # metrics=[dice_2d]

    return model


###############################
# Misc
###############################

def save_history(hist, params, cv, results_path):
    x = range(1, len(hist['categorical_crossentropy']) + 1)
    plt.figure(figsize=(12, 12))
    plt.plot(x, hist['categorical_crossentropy'], 'o-', label='loss')  # dice_2d
    plt.plot(x, hist['val_categorical_crossentropy'], 'o-', label='val_loss')  # val_dice_2d
    plt.legend(loc='upper left')
    plt.ylabel('loss')
    plt.grid(True)

    plt.savefig(results_path + '/firstval' + str(cv['val'][0]) + '/learning_curves.png')
    plt.close()


def params2name(params):
    results_name = ''
    for key in params.keys():
        results_name = results_name + key + '_' + str(params[key]) + '_'
    results_name = results_name[:-1]
    return results_name


def save_params(params, path):
    if not os.path.exists(path):
        os.mkdir(path)
    pickle.dump(params, open(path + '/params.p', "wb"))
    with open(path + '/params.csv', 'w') as csv_file:
        writer = csv.writer(csv_file)
        for key, value in params.items():
            writer.writerow([key, value])


def cv_index_generator(params, nb_train_max, results_path, fold_index=0, en_print=True):
    fa = params['factor_augment']
    ind_trainvaltest = nb_train_max * fa + int((params['nVal'] + params['nTest']) * fa)
    ind_trainval = nb_train_max * fa + int(params['nVal'] * fa)
    ind_train = nb_train_max * fa
    if params['nTest'] != 0:
        listOfIndices = np.roll(np.arange(ind_trainvaltest), -fold_index * params['nTest'] * fa)
    else:
        listOfIndices = np.roll(np.arange(ind_trainvaltest), -fold_index * params['nVal'] * fa)
    trainList = listOfIndices[0:params['nTrain']]
    valList = listOfIndices[ind_train:ind_trainval:fa]
    testList = listOfIndices[ind_trainval:ind_trainvaltest:fa]
    cv = {'train': trainList, 'val': valList, 'test': testList, 'cvNum': fold_index}

    if en_print:
        print(cv['train'])
        print(cv['val'])
        print(cv['test'])

    if not os.path.exists(results_path + '/firstval' + str(cv['val'][0])):
        os.makedirs(results_path + '/firstval' + str(cv['val'][0]))
    pickle.dump(cv, open(results_path + '/firstval' + str(cv['val'][0]) + '/cv.p', "wb"))

    return cv


def jaccard(im1, im2):
    """
    Computes the Jaccard metric, a measure of set similarity.
    Parameters
    ----------
    im1 : array-like, bool
        Any array of arbitrary size. If not boolean, will be converted.
    im2 : array-like, bool
        Any other array of identical size. If not boolean, will be converted.
    Returns
    -------
    jaccard : float
        Jaccard metric returned is a float on range [0,1].
        Maximum similarity = 1
        No similarity = 0

    Notes
    -----
    The order of inputs for `jaccard` is irrelevant. The result will be
    identical if `im1` and `im2` are switched.
    """
    im1 = np.asarray(im1).astype(np.bool)
    im2 = np.asarray(im2).astype(np.bool)

    if im1.shape != im2.shape:
        raise ValueError("Shape mismatch: im1 and im2 must have the same shape.")

    intersection = np.logical_and(im1, im2)

    union = np.logical_or(im1, im2)

    return intersection.sum() / float(union.sum())