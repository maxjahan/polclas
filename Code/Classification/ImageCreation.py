import cv2
import pickle
import os
import imgaug as ia
import imgaug.augmenters as iaa
import random
import transforms
from matplotlib import pyplot as plt
import keras.utils
from transforms import *
from imgaug.augmentables.segmaps import SegmentationMapsOnImage
import time  # only if you want to track the processing time
import numpy as np  # for matrix manipulation
from PIL import Image  # for loading image files
from multiprocessing import Pool  # if you want to leverage multi-core cpu(s)
import glob  # this is for listing all the files in a folder
from random import randint
from run import nbClasses, nTrain, nVal, nTest, nTrainMixed


def randomCrop(img, mask, width, height, margin):
    assert img.shape[0] >= height
    assert img.shape[1] >= width
    assert img.shape[0] == mask.shape[0]
    assert img.shape[1] == mask.shape[1]
    x = random.randint(0, img.shape[1] - width - margin)
    y = random.randint(0, img.shape[0] - height - margin)
    newIm = img[y:y + height, x:x + width]
    newMask = mask[y:y + height, x:x + width]  # ,newMask
    return newIm, newMask


def getTestImage(height, width):
    files = os.listdir('dataFinal/Mixed')
    d = random.choice(files)
    baseTest = glob.glob('dataFinal/Mixed/' + str(d) + '/Base/*')
    maskTest = glob.glob('dataFinal/Mixed/' + str(d) + '/Mask/*')
    imBase = cv2.imread(baseTest[0])
    imMask = cv2.imread(maskTest[0])
    imBase = cv2.cvtColor(imBase, cv2.COLOR_BGR2RGB)
    imMask = cv2.bitwise_not(cv2.cvtColor(imMask, cv2.COLOR_BGR2GRAY))
    imMask = np.divide(imMask, 255 / int(d))
    imMask = np.reshape(imMask, (4000, 4000, 1))
    cropped_baseTest, cropped_maskTest = randomCrop(imBase, imMask, height, width, 10)
    if np.all(cropped_maskTest == 0):
        return getTestImage(height, width)
    return cropped_baseTest, cropped_maskTest


def getImage(baseImage, maskImage):
    cr_base, cr_mask = randomCrop(baseImage, maskImage, W, H, 30)
    if np.all(cr_mask == 0):
        getImage(baseImage, maskImage)
    return cr_base, cr_mask

showTest = False
nbr_classes = 12   #+1 for background
W = 800
H = 800

numberRandom = math.ceil((nTrain+nVal)/nbClasses)
print('---------  nbRandom = ' + str(numberRandom) + '  ----------')



imPerRow, imPerCol = 2, 2
Wtest, Htest = int(W / imPerRow), int(H / imPerCol)
print(Wtest)

masks = []
base = []

files = glob.glob("dataFinal/Mixed/*")

for file in files:
    filesB = glob.glob(file + "/Base/*")
    filesM = glob.glob(file + "/Mask/*")
    pathname = os.path.basename(file)
    # print(pathname)

    for myFileMask, myFileBase in zip(filesM, filesB):  # loop for mask files
        maskImage = cv2.imread(myFileMask)
        baseImage = cv2.imread(myFileBase)

        # ======================================== Augmentation start ========================================

        # rotate = transforms.Rotate(limit=45, prob=0.5)
        # baseImage, maskImage = rotate(baseImage, maskImage)
        # baseImage, maskImage = augment_a_little(baseImage,maskImage)
        baseImage, maskImage = augment_color(baseImage, maskImage)

        # ======================================== Augmentation end ========================================

        maskImage = cv2.bitwise_not(cv2.cvtColor(maskImage, cv2.COLOR_BGR2GRAY))
        maskImage = np.divide(maskImage, 255 / int(pathname))
        maskImage = np.reshape(maskImage, (4000, 4000, 1))

        baseImage = cv2.cvtColor(baseImage, cv2.COLOR_BGR2RGB)

        for i in range(numberRandom):  # Create numberRandom image crops for each image
            cropped_base, cropped_mask = getImage(baseImage,maskImage)
            base.append(cropped_base)
            masks.append(cropped_mask)

# ------------------ Test image creation ------------------------  #

for t in range(nTest+nTrainMixed):            #We create mixed images for test & train data
    masktest = np.zeros((W, H, 1))
    test = np.zeros((W, H, 3))
    for x in range(0, imPerRow):
        for y in range(0, imPerCol):
            cropped_baseTest, cropped_maskTest = getTestImage(Htest, Wtest)
            test[x * Wtest:x * Wtest + Wtest, y * Htest:y * Htest + Htest] = cropped_baseTest
            masktest[x * Wtest:x * Wtest + Wtest, y * Htest:y * Htest + Htest] = cropped_maskTest
    base.append(test)
    masks.append(masktest)
    print('---------  Test Mozaic ' + str(t) + '----------')
# -------------------------------------------------------------------


masks = np.array(masks)
base = np.array(base)
labels = keras.utils.to_categorical(y=masks, num_classes=12)

# -------------- Shuffle the images----------------------------------
indices = np.arange(masks.shape[0] - nTest)
testindic = np.arange(masks.shape[0] - nTest, masks.shape[0])
np.random.shuffle(indices)  # We shuffle only the training images
indices = np.concatenate((indices, testindic))
masks = masks[indices]
base = base[indices]
labels = labels[indices]
# -------------------------------------------

print('mask shape: ' + str(masks.shape))
print('base shape: ' + str(base.shape))
print('labels shape: ' + str(labels.shape))

# saving the .npy files
np.save('samples/masks', masks)
np.save('samples/base', base)
print("saved base")
np.save('samples/labels', labels)

maskSh = masks.shape
truth = np.zeros((maskSh[0], nbr_classes))

for i in range(maskSh[0]-nTest,maskSh[0]):  # For each image,

    backG = np.count_nonzero((masks[i, :, :, 0]) == 0)
    print(backG)
    truth[i, 0] = np.divide(backG, (maskSh[1] * maskSh[2]))
    for j in range(1, nbr_classes):  # And for each class we sum the probabilities
        sum = np.zeros(nbr_classes)
        sum[j] = np.count_nonzero((masks[i, :, :, 0]) == j)
        sum[j] = np.divide(sum[j], (maskSh[1] * maskSh[2]) - backG)
        truth[i, j] = sum[j]

np.save('samples/truth', truth)
print("saved truth")

if showTest:
    for i in range(nTrainMixed + numberRandom * nbClasses, nTest + nTrainMixed + numberRandom * nbClasses):  #
        f, axarr = plt.subplots(1, 2)
        axarr[0].imshow(np.uint8(base[i]))
        axarr[0].title.set_text('Test images')
        axarr[1].imshow(masks[i, :, :, 0])

        axarr[1].title.set_text(str(round(100 * truth[i, 0], 2)) + '% background')
        plt.show()
        print(i)
        time.sleep(0.001)
