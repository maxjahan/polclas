from utils import *
from train import *
import numpy as np

gpu = 1
os.environ["CUDA_VISIBLE_DEVICES"] = str(gpu)


def predict_model(images, params, nbr_classes, cv, results_path, en_save=True):
    # Get model
    if params['model'] == 'unet_2d':
        model = unet_2d(params)
    model.load_weights(results_path + '/firstval' + str(cv['val'][0]) + '/weights.h5')

    # Load normalization parameters
    pickle_in = open(results_path + '/firstval' + str(cv['val'][0]) + '/norm_params.p', "rb")
    norm_params = pickle.load(pickle_in)

    # Perform prediction
    sh = images.shape
    if nbr_classes > 1:
        predictions = np.zeros((sh[0], sh[1], sh[2], nbr_classes))
        result = np.zeros((sh[0], sh[1], sh[2], nbr_classes))
    else:
        predictions = np.zeros((sh[0], sh[1], sh[2]))
    # predictions_thr = np.copy(predictions)

    for i in range(sh[0]):
        test_inputs_i = (images[i, :, :] - norm_params['mu']) / norm_params['sigma']
        test_inputs_i = np.expand_dims(test_inputs_i, axis=0)
        test_predictions_i = model.predict(test_inputs_i, batch_size=params['batch_size'], verbose=0)
        predictions[i] = np.squeeze(test_predictions_i)


    for i in range(sh[0]):
        for x in range(sh[1]):
            for y in range(sh[2]):
                indic = np.argmax(predictions[i, x, y])
                result[i, x, y, indic] = 1

    pred = np.zeros((sh[0], nbr_classes))

    for i in range(sh[0]):  # For each image,
        backG = np.sum((result[i, :, :, 0]))
        pred[i, 0] = np.divide(backG,(sh[1] * sh[2]))
        for j in range(1, nbr_classes):  # And for each class we sum the probabilities
            sum = np.zeros(nbr_classes)
            sum[j] = np.sum(result[i, :, :, j])
            sum[j] = np.divide(sum[j], (sh[1] * sh[2])- backG)
            pred[i, j] = sum[j]

    if en_save:
        np.save(results_path + '/firstval' + str(cv['val'][0]) + '/prediction.npy', predictions)
        np.save(results_path + '/firstval' + str(cv['val'][0]) + '/predGen.npy', pred)
        np.save(results_path + '/firstval' + str(cv['val'][0]) + '/result.npy', result)
