
In this final chapter, we illustrate the application of our implemented U-net classification model on our data-set. First of all, section \ref{sec:valid} describes the way generate the testing set our model. Then, we detail the choice of hyper-parameters for the classification model. Section \ref{sec:res} graphically illustrate and discusses the predicted results of our computed model on the testing set.




\section{Validation method} \label{sec:valid}

To be able to test our model, there are two very major things we need to acquire. Primarily, we need a testing set , and secondly we need to compile the model. Let us first start with the less complex one, being the former.

\subsection{Testing set}
%  DATASET
Even though our data-set is composed of images of pure pollen, one of the goals of the model is the capacity of differentiating types of pollen grains when they are mixed together. This is why the need of images composed of multiple types of pollen is so important. Unfortunately, due to time constraints, we were not able to acquire mixed images that corresponded with the species of our data-set of pure pollen images. Thankfully, we were able to synthesise mixed images by ourselves by tinkering with the pure images. We composed these mixed images by arbitrarily picking a pollen species from the data-set, and then cropping a random, non background part of that image. To be able to have to most uncropped pollen grains on the images, we chose to create 2 by 2 matrices of different images. An example of such an synthesised mixed image can be seen in figure \ref{fig:mozaic}. 

\begin{figure}[t!]
\includegraphics[width=1.\linewidth]{images/mozaic.png}
\centering
\caption{A synthesised image of multiple different pollen species(left), with it corresponding mask(right). From top to bottom and left to right : Arabidopsis thaliana, Arrabis hirsuta, Genista pilosa, Crambe maritima }
\label{fig:mozaic}
\end{figure}

The drawbacks of these kind of synthesised images compared to real mixed images, is that sometimes we can get awkward, unnatural crops that will hinder the identification of the right pollen species. A second drawback is that we get non-uniform background that will again complicate the prediction. But this problem can thankfully be partially removed by adding a few composite images to the training data, to teach the model to ignore the often sudden contrast changes at the borders of the regions. A third problem is that, by randomly selecting species, we could get an imbalance in the size of the classes.

\subsection{Model }
% Number classes
% Learning rate (decay) step size
%n feature maps
% nlayers
% Epochs ? 200
% Loss f/activ

When compiling a model, even with a given architecture and data-set, there are a dozen of hyper-parameters that will influence the capabilities of the model. In these following paragraphs, we go over the exact values of the ones we chose for the validation of our proof-of-work.  


\paragraph{Classes}

We chose to train our model with a total of \textbf{11 different species} of pollen grains. From the data that was at our disposal, we tried to select species that were visually as diverse as possible. 

\paragraph{Training, Validation and Testing sets}

The compiled U-net classification model was fit on 170 images, composed of 99 pure pollen images, and 60 synthesised mixed images. It is fine-tuned with a validation data-set, composed of 33 images that are - following the ratio of the training set - either mixed or pure images. Finally, the testing set is composed of 30 synthesised mixed images. All images have the same dimensions : 800 by 800 pixels.
This ratio split of sets gets us at \textbf{70\% / 15\% / 15\%}, which is a classic ratio split in machine learning. 


\paragraph{Learning rate}
The learning rate is perhaps one of the most important, if not the most important parameter to configure correctly for a model. A correct learning rate can only be guessed through experiencing with the model. An even better way to improve the time to convergence, is to decrease the learning rate as the model advances into the algorithm : an adaptive learning rate.
We found out that starting with a \textbf{learning rate of $2e^{-4}$} was a good way to rapidly arrive at convergence. We also chose a decaying factor of 0.5, and a step size of 20. In other words, our learning rate is halved every 20 steps.

\paragraph{Epochs and Batches}

The number of epochs, corresponding to the number of times the algorithm goes trough the data to update the weights, was set to \textbf{100 epochs}. This choice is based on the observation of the loss curves. By plotting the loss with respect to the number of epochs, we can see that it has converged after a hundred epochs. We compiled the model while working on 2 different batches, to ensure the convergence of the model was not a lucky draw/

\paragraph{Layers}

As stated before, to ensure that our model has a large enough receptive field to recognise pollen grains from other background elements, we constructed a U-net composed of \textbf{5 layers}.

\paragraph{Activation and Loss functions}

For our multi-label pollen classification network, we chose to opt for a \textbf{SoftMax} activation function, combined with a \textbf{categorical cross-entropy} loss function. 


\subsection{Test results measurement method}

In order to be able to quantitatively quantify our results, we need a way gauge the success(or failure) of our model. We have to keep in mind that because the images were manually annotated, we can not be certain that the annotated mask are the "real" truth. The error is human, and that means that only comparing the pixel values directly wont necessarily give us the exact performance of the network. Thus is why we will also judge based on visual inspection. 
But even with visual proof, we need quantitative proof that our model works.
To achieve this, we will measure a few different statistics. 

To validate the classification capabilities of the network, we will also compare the ratio of the predicted pollen species in the test images, with the ratios of the masks. We inspect the ratios to compare if the most present pollen types in the prediction are the same as the one in the masks. This is a somewhat similar approach as the Jaccard coefficient described below, but it is simpler and a quicker approach.

\subsubsection{Jaccard similarity coefficient}

The Jaccard similarity index, or coefficient, measures the similarity between two sets of data. It is a very popular measuring statistic, as it is efficient and very simple to understand. The value range goes from 0, a really bad classification, to 1, a perfect one.
The coefficient is formally defined as the size of the intersection of the two sets, divided by the size of the union of the two sets. A mathematical representation of the Jaccard coefficient is presented in figure \ref{fig:Jac}.

\begin{figure}[h!]
\includegraphics[width=0.45\linewidth]{images/Jac.png}
\centering
\caption{A mathematical representation of the Jaccard similarity coefficient}
\label{fig:Jac}
\end{figure}




\subsubsection{Matthews correlation coefficient}

We will finally measure our classifier with one last instrument.
The Matthews correlation coefficient(MCC), or phi coefficient is a popular and reliable statistic that measures the quality of a binary or multi-class classifier. Unlike the Jaccard index, it takes true and false positives and negatives. It handles unbalanced data-sets better, and is thus a better measure when the classes differ a lot in size. The formula for the computation of this statistic can be found in equation \ref{Matt}. The MCC has a range going from -1 to 1, with -1 corresponding with a really bad classifier, and 1 indicating a completely correct one. We will use this coefficient to compare the predicted images with their ground truth. 


\begin{equ}[!ht]
    
   
$$MCC = \frac{TP*TN -FP*FN}{\sqrt{(TP+FP)*(TP+FN)*(TN+FP)*(TN+FN)}}$$
    

\caption{A mathematical representation of the Matthew correlation coefficient. TP = True positive, TN = True negative, FP = False positive, FN = False Negative}
 \label{Matt}    
\end{equ}    

    








\paragraph{Summary}
To conclude, in addition to visual inspection, we will judge our model's performance by calculating and analysing the jaccard similarity coefficients for each class. This will give us a measure of the similarities between the true value and the prediction set. Subsequently, we will measure the capabilities of our classification algorithm with the Matthews correlation coefficient, which is a bit more reliable as it takes a few more elements into account than the jaccard index,like true and false positives. 