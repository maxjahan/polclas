%
% Intro 
%

\chapter*{Introduction}
\markboth{INTRODUCTION}{}
\addcontentsline{toc}{chapter}{Introduction}

\pagenumbering{arabic}
\setcounter{page}{1}

%1. présentation du problème
%2. montrer que le problème est important, par exemple en présentant des applications
%3. présenter l’état de l’art pour le problème en insistant sur :
%4. contribution du travail par rapport à l’état de l’art
%5. Structuration du document







% This report is structured in the following way:
% the problem and the current state-of-the-art are presented in Chapter~\ref{ch:LRMC}.

% why what how 
%1. présentation du problème
%2. montrer que le problème est important, par exemple en présentant des applications

Palynology is a very peculiar field of study within the realm of ecology. Dealing with pollen and spores of plant species, it may be one of science's most underrecognised branches. It may look like a very niche and narrow biological field, but is has uses in a myriad of other environments. It is used paleobotany and melissopalynology, where palynology can reveal lots of information about past and current local ecosystems. Melissopalynology is a field that studies honey and pollen to gain biological and geographical knowledge about the local flora. Another field of interest tied to palynology is hydrology, where pollen, spores or specific sediments can help determine above or underground water systems. A last example is the use of pollen samples in forensic science to help criminal investigations, by bringing a unique kind of evidence to the table.


The automatization of the process of classifying pollen grains is an important issue that would save a ton of time and resources. Going from being beneficial in the healthcare industry for allergy-related systems to mapping entire ecosystems, pollen grain classification can be useful in hundreds of domains. 

Only a few decades ago, biologists still used light microscopes to identify and quantify pollen grains. After huge technological advances in computer science, imaging techniques and imaging hardware, the complete automation of classifying pollen with almost no human interaction is becoming achievable.
Most classification models are composed of a crucial component to be able to differentiate one type of constituent from another: \textit{discriminant features}. 
Discriminant features represent the specific attributes and characteristics of the object of interest. These characteristics will be used by the algorithm to create a mathematical model which will be able to discern one object of interest from the other. A big problem with palynology is that different types of pollen are \textit{really} similar\cite{doi:10.1111/nph.12848}. This makes the algorithms very sensitive to parameter variation. The design and selection of these kind of features are a time-consuming task. Recently, deep learning algorithms have emerged. These, more complex kind of algorithms, are able to learn these features by themselves, without humans intervening, thus saving them from a tedious and time-consuming task. 

%3. Current State of art:

A lot of authors have tried to solve the problem of pollen classification, based on the manual extraction of features. A notorious paper \cite{statestillman} from 1996 published a brief summary of the state of the art until then, and more importantly the demands and needs of palynology to elevate the field to a higher level, thus making it a more powerful and useful tool. These needs can be summarised in distinct categories. First of all, the need for more data, more pollen grains per sample and more data-sets. Secondly, the need for faster analysis. To put an order of magnitude on it, in 1995 it took a trained researcher between 2-10 hrs to analyse a sample. Finally, another improvement field would be resolution. Fine-resolution palynology could for example discover climate shifts.  

As of today, manual extraction of discriminant features remains a used technique. We can divide this kind of analysis into several subcategories. \cite{stateSum1}

\nomenclature{SVM}{Support-Vector Machine}

\begin{itemize}

    \item Morphological methods: 
Here visual features like colour, shape or symmetry are measured. Treloar \& coll.\cite{state1} tried to work on a solution based on visual features like grain perimeter, roundness and area, then applying a Fisher discriminant. They classified 12 different pollen types from a Polynesian island, with a classification rate up to 95\%. With SVM's as classifier,\cite{state3} used morphological details of the pollen grains contour as discriminative features. They achieved 93.8\% ± 1.43 of success while classifying 47 tropical honey plants.


    \item Texture-based methods: 
These approaches make use of the characteristics of the pollen grain surface as discriminative feature. As an example, \cite{state5} presents a system that detects and classifies pollen grains based on a combination of shape and texture. An Urticaceae  data-set, which are very similar family, are used for performance evaluation. They achieved 89\% success.
Alternatively, based on grey-level co-occurrence matrices and neighbourhood grey level dependence statistics, \cite{state2} were able to classify 5 different pollen-types with more ore less 75\% classification success. 
\end{itemize}

An alternative to the manual extraction of the features would be to let an algorithm learn these by itself by training it to fit a data-set. This can be done through deep learning. Deep learning is a subset of machine learning, inspired by the human brain. It uses a hierarchical structure with a series of layers to create neural networks. The more layers the network has, the more complex the model can get. While the lower layers will identify simple features such as curves or straight lines, the higher levels will be able to extract more complex characteristics.
The hierarchical nature of the structure allows it to analyse data non-linearly, in contrary to traditional algorithms. A downside to these neural networks is that they need a consequent amount of training data and computing power.

Whether it is via manual or automatic extraction of features, when comparing results of different studies, we have to bear in mind that the degree of success will be hugely influenced by plenty of parameters. Not only the similarity between the pollen species but also the quality of the images or the amount of input images will greatly increase the accuracy of the model.

% spécificités de la thèse : majorités des études partent d'un data-set de donées MIXTES, 
% notre travail part de lames PURES 

Existing state-of-the-art articles and research on pollen grain classification conventionally rely on data-sets constituted of scans composed of multiple species of pollen. 
In this work, we present a solution to the pixel-wise identification and classification of pollen grains by automatic extraction of the discriminant features, using  a convolutional neural network. The specificity of this thesis compared to other existing work, and thus its contribution to the state of the art, lies in the data-set. Indeed, typically the problem is addressed by training a convolutional network with images containing a number of different species of pollen grains. This method has in reality many drawbacks, as it involves the quite time-consuming task of annotating each scan for the training data. Not only is it tedious, but it also demands a certain level of expertise, as the person annotating must also know how to differentiate the grains by sight. 

On the contrary, in this thesis we are dealing with a data-set composed of scans with pure pollen. In other words, every image in our training set consists of only one species of pollen. This strategy has quite a few advantages. In the first place, this technique need a lot less manual annotation, so it is less time-consuming compared to classic approaches.
Secondly, the fact we are able to train the data with images of pure pollen, makes the algorithm modular. This means that at any moment, we can just add a few annotated scans of a chosen species, launch the algorithm, and the model will be able to recognise the new pollen species. Whereas conversely, we would have to first obtain and then annotate images of mixed pollen species, which would take a lot more time and effort. 

We will use these advantages cited above to attempt to create a powerful and efficient classification tool. The strategy utilised to address this classification problem is the following:
Having manually annotated a few dozen images of pure pollen grains, we will first construct a pixel-wise segmentation CNN to facilitate the annotation of future supplementary data. The second step will be to implement the pixel-wise, multi-label classification algorithm. We will do this by basing our architecture on the well-known and efficient U-net network. By training this network with our pure pollen images, we will then get a model that will be able to predict mixed images. To get a clearer picture of the strategy described precedently, a bloc-diagram in figure \ref{fig:Roadmap} illustrates how the problem is tackled.

\begin{figure}[h!]
\includegraphics[width=1\linewidth]{images/Roadmap.png}
\centering
\caption{An illustration of the strategy used to address the problem presented in this thesis}
\label{fig:Roadmap}
\end{figure}


% Thus, this master's thesis aims at answering the following question : \textit{Given images of multiple pure pollen species, is it possible and to create a tool that is able to pixel-wise identify and classify the grains into multiple classes?}

This report is structured in the following way: In chapter \ref{ch:BG}, we start by going over the basics of neural networks. We then more specifically describe the functioning of convolutional networks, and some important concepts related to the subject. Then, in chapter \ref{ch:ImFormat}, the characteristics of the data-set are detailed. Further, in chapter \ref{ch:meth}, we go over the specific architecture used for our neural network, explaining its main components, advantages and drawbacks. In this same chapter, we will detail the design of our segmentation CNN, followed by the workings of the pixel-wise classification CNN. Lastly, in the final chapter (Ch\ref{ch:Val}), we go over the results of our work. First by explaining the way we will be evaluating our method, then reviewing and discussing our findings in the last section.   

% structure of the report







Following the discipline of reproducible research, the source code and data files required to reproduce the experimental results of this master's thesis can be downloaded from \url{https://bitbucket.org/maxjahan/polclas/src/master/}. 


