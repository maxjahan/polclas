% nomenclature
\nomenclature{CNN}{Convolutional Neural Network}
\nomenclature{NN}{(Artificial) Neural Network}
\nomenclature{MLE}{Maximum Likelihood Estimation}
\nomenclature{MSE}{Mean Squared Error}
\nomenclature{ReLU}{Rectified Linear Unit}
\nomenclature{MCC}{Matthews Correlation Coefficient}


% 
The task of classifying pollen grains could be learned by any individual trained with basic biologic knowledge. This skill of observing an image and being able to categorise its content into semantically meaningful objects, is an aptitude a human learns since (s)he is a child. The first time a child sees a dog, he can't differentiate it from a cat. But with enough data, after having seen thousands of each species, any capable human can differentiate them from a hundred meters away. The human brain is constantly creating new connections between neurons, associating context elements, learning patterns and thus expanding our perception of the environment.

It is exactly this kind of learning that is reproduced in neural networks. These algorithms, inspired from the human brain, are designed to recognise patterns. They are able to learn relationships between specific characteristics, make generalisations, inferences, and even reveal relations that would be hidden to the human eye. This is why neural networks are such a powerful tool for image classification.

The human brain consists of hundreds of millions of neurons, connected via synapses. When a sufficient amount of synapses connected to a certain neuron are activated, this neuron will fire and send an outgoing signal via output synapses. The way these millions of neurons and synapses are connected shape the way we think and act.

In neural networks, the neurons are replaced by artificial neurons, and they are connected with each-other by weighted synapses. The activation of the artificial neuron will depend on:

\begin{itemize}
    \item The weights used to combine the neuron inputs. These weights will change as the neural network will learn.
    \item An activation function, this can be a simple threshold for example.
\end{itemize}


The neurons are organised in layers, and each neuron from an arbitrary layer can be influenced by the neurons of the previous layer by taking input from it. In figure \ref{fig:NNex1} we can see how one neuron is influenced by neurons of a previous layer. The point of such an architecture, composed of a series of layers consisting of multiple neurons, is to help us cluster and classify data. The neural network will approximate a function \textit{f(x) = y} for a given input x, and output y. With the help of training data, the function will learn to fit the training set. The training of the network is done by minimising a loss function(see section \ref{subsec:loss}) by adjusting the weights of the neurons. After being trained, the network will approximate a function that will be able to predict an output y' for some given data x'.

\begin{figure}[h!]
\includegraphics[width=0.65\linewidth]{images/NNex1.png}
\centering
\caption{Illustration of the relations between artificial neurons in a neural network. We can see that the 3 lowest neurons, constituting the input layer, are giving an input to the upper neuron . }
\label{fig:NNex1}
\end{figure}


An example of a small neural network constituted of only 2 layers is depicted in figure \ref{fig:NNex1}. As the number of layers grow, the complexity of the model also grows. In fact, more than 3 layers is enough to qualify as a "deep" learning algorithm. In deep learning, the number of layers can even go up to a thousand. 
When talking about layers in a neural network, and modelling its structure, we can define a few different types of layers. The following different kinds of layers are depicted in figure \ref{fig:NNex2}: 

\begin{itemize}
    \item \textbf{The input layer} is the first layer, providing the input data to the network.  
    \item \textbf{The output layer} is the last layer, giving us the predicted result.
    \item \textbf{The hidden layers} are all the layers between the first and the last one. Every layer can apply a function to the previous layer. As the layers increase, so does the level of abstraction. For example, in facial recognition , the first hidden layers could be used to identify a nose or an eye, while the last hidden layers will be able to classify a whole face. This hierarchy in features is what enables the neural network to analyse very large and high-dimensional data sets. 
    The job of the hidden layers is to gradually transform the input data into a feature the output layer can use. 
    
\end{itemize}

\begin{figure}[h!]
\includegraphics[width=0.65\linewidth]{images/connected.jpeg}
\centering
\caption{Illustration of the different types of layers in a NN. This network is fully connected. The red neurons represent the input layer, the blue neurons form the hidden layers, while the green neuron constitutes the output layer.\cite{stanImage}}
\label{fig:NNex2}
\end{figure}


\paragraph{Fully Connected layer} In neural networks, when all the inputs from one layer are connected to every node of the next layer, we call this fully connected layers. Following this definition, the network depicted in figure \ref{fig:NNex2} is fully connected.





% ####################################################### 

\section{Key-concepts of a neural network} \label{sec:concepts}

To summarise, neural networks are composed of several layers of nodes, that will influence each other based on the weights given to each of these nodes. To train a neural network, an optimization function will compute the error between the network's prediction and the ground-truth. Based on that error, it will recompute and adjust the weights using an iterative gradient-descent algorithm. With good parameters, after a certain amount of iterations(or epochs, in machine learning slang), the error is minimised and, hopefully, the model becomes able to generalise unseen data. An example of a potential real-life application of a NN to predict lung cancer is depicted in figure \ref{fig:concept2}. 


\begin{figure}[h!]
\includegraphics[width=0.9\linewidth]{images/Blank Diagram.png}
\centering
\caption{Potential application of a neural network. The goal is to predict the probability of a random person getting lung cancer,  given his/her smoking habits, the endured chest radiation and his/her genetic predispositions. This network example could be extended by adding a lot more risk factors, making it more precise in it's prediction}
\label{fig:concept2}
\end{figure}


\subsection{Gradient-descent}
The term used for the most popular and commonly used optimization function is "gradient descent". This is the function that will adjust the weights on the nodes to optimize the result based on the error. Let us now briefly explain the way gradient descent works. We can comprehend this more easily by comparing and picturing it as a mathematician hiking in a mountainous region.

Initially, all weight coefficients are randomly assigned. We can imagine this by parachuting the little man on a random location on the region. Then, based on a certain loss/cost function, the algorithm will calculate the parameter variation that results in the highest decrease in error. This calculation will repeat itself until we arrive at a local minimum. Let's now get back to our little guy.
So, the man has a very short sight and can thus only see the next step he could take around him. He will always choose the steepest path (downhill) he can take. After enough steps taken, he will slowly get to the lowest point in the region. Once he realizes that taking steps doesn't get him much lower, and he is stuck in the same little spot for a while, he stops walking. This would be the convergence of the algorithm.

Taking figure \ref{fig:GradDes}, we could also illustrate this by imagining a ball  that is placed on the top of the mountain. It will slowly roll down the mountain and eventually end up in one of the lower, blue areas, a local minimum. This also demonstrates that, depending on the initial placement of the ball, it could end up in a different place. This is also one of the biggest drawbacks of this algorithm : the possibility of getting stuck on a local minimum.



\begin{figure}[h!]
\includegraphics[width=0.42\linewidth]{images/GradDes.png}
\centering
\caption{Illustration of the gradient descent method \cite{CNNDoc3}}
\label{fig:GradDes}
\end{figure}

% source : https://ml-cheatsheet.readthedocs.io/en/latest/gradient_descent.html


\subsection{Learning rate}

The learning rate is another important parameter that will impact the efficiency of a deep learning algorithm. It influences the rate at which the weights are updated per iteration. So, a high learning rate will adapt the weights quicker, thus requiring less iterations of the algorithm, but could overshoot the solution and converge to a sub-optimal solution. At the other end, a learning rate that is too small could extend the number of iterations to an unreasonable amount. Moreover, it has been shown experimentally that, for the same amount of epochs, small learning rates lead to poorer generalisation capabilities \cite{LR}. The better choice is almost always to pick an adaptive learning rate. This means the learning rate will vary over the training process. This can be implemented easily by halving the value of the learning rate every X amount of epochs. An even more complex approach could be taken, like the one Tom Schaul et al.(2013) presented in their method with a learning rate able to decrease or increase during the learning process, making it suitable for non-stationary problems \cite{LRart}.

Going back to our analogy of a mathematician walking in the mountains, we can compare the learning rate to the size of his steps. A high learning rate would mean he takes huge steps, descending the mountains very quickly, but adding the risk of overshooting the lowest point of the valley and thus missing the optimal weight choice for the network. A very tiny step-size would mean the mathematician would take days, or even months to get to a minimum. This would result in the man getting out of supplies and having to end his journey before being able to finish it. 

\subsection{Loss functions} \label{subsec:loss}

When training a neural network using the gradient descent optimization algorithm, it has to compute the error for each iteration. This evidently calls the need for an error function, also called loss function. The choice of this loss function will influence the efficiency of the algorithm. There are many possibilities here and choosing the right loss function can be challenging.\cite{Loss1}

A very popular way of choosing this function is using the concept of Maximum Likelihood Estimation, or MLE. MLE is a probabilistic framework that can be used for finding the best statistical estimation for  model parameters, based on training data. \cite{bookNeural1} The loss function under such a framework, will compare how close the predicted distribution of parameters are to the distribution of the parameters in the training set. One important property of MLE functions is consistency. This means that, with increasing data, the parameters will converge to their true data. In deep-learning, a very popular loss function for binary or multi-class classification problems is Cross-Entropy. This is a measure coming from the field of information theory. It is used to measure the difference between two probabilistic distributions. The most popular loss function, for regression problems, is the Mean squared error or MSE function. As the name indicates, it computes the average of the squared differences between the predicted and true values. The results will thus always be positive. Due to the square, small differences will be less notable, while big mistakes in the model's prediction will be punished harsher and results in big errors.

\subsection{Activation functions}
The activation function also has a major role to play in the efficiency of a neural network. Basically, the activation function is an mathematical equation, determining the output of a neuron or node. It determines if the neuron "fires" or not. It acts like a mathematical gate between the input, and the next layer.
Almost all modern neural networks now use non-linear activation function, enabling it to learn complex data, and make precise predictions. A few of the most popular functions are depicted in figure \ref{fig:AF}. The sigmoid function (fig. \ref{fig:sigm}) is often used, as it is quite simple to compute, and output values are bound and thus don't get too big. A drawback of the sigmoid function and other classic activation functions is that it suffers from the vanishing gradient problem. In short, when using gradient descent with such activation functions in deep neural networks, the error will decrease abruptly with every layer because of the derivative of the chosen function. \cite{Act1}\cite{Act2} \cite{Act4}

The Rectified Linear Unit or ReLU is an alternative that (figure \ref{fig:relu}) will output the input directly if positive, and will otherwise output zero. Because of it ability to cope with the vanishing gradient problem, and its great performances, it is the default activation function for many NN.

A last and important activation function is the \textit{Softmax}. Like the sigmoid, the softmax is a function based on logistic regression. 
The softmax turns the scores into a normalised, probability distribution. This means that sum of the scores for all classes is equal to 1. Thus, this kind of activation function is useful when the output classes are mutually exclusive(there can be only one right answer) \cite{Act3}. 

\begin{figure}[h!]
    \centering
    \begin{subfigure}[t]{0.5\textwidth}
     \includegraphics[width=0.9\linewidth]{images/sigm.png}
    %  \centering
     \caption{Sigmoid Activation function}
     \label{fig:sigm}
    \end{subfigure}%
~ 
    \begin{subfigure}[t]{0.5\textwidth}
      \includegraphics[width=0.9\linewidth]{images/relu.png}
    %   \centering
      \caption{ReLU activation function}
      \label{fig:relu}
    \end{subfigure}
\caption{Several popular activation functions \cite{Act2}}
\label{fig:AF}
\end{figure}

% https://missinglink.ai/guides/neural-network-concepts/7-types-neural-network-activation-functions-right/
% https://medium.com/@srnghn/deep-learning-overview-of-neurons-and-activation-functions-1d98286cf1e4

% \subsubsection{Back-propagation}

\section{Convolutional Neural networks} \label{sec:CNN}

In the field of visual imagery, we often use a special kind of neural network, specifically adapted to the analysis and processing of image data-sets. They are called Convolutional Neural Networks (CNNs). The same way neural networks are based on the biological structure of the human brain, CNNs are modeled after the visual cortex. The first layers of neurons each have a spatial expertise, meaning that every neuron responds to stimuli in a specific area, the receptive field of a neuron. This enables the network to detect patterns, like edges or lines. With a growing number of layers, these patterns increase in complexity, and can allow a CNN to distinguish a tree from a car, or even a man from a woman. 
But what exactly makes a Convolutional neural network different from a classic, multi-layer network? \cite{CNNbook1}\cite{CNNDoc1}\cite{CNNDoc2}\cite{CNNDoc4}

\subsection{Convolutional layer}
Well, the answer is in the name, they have convolutional layers. In these layers, we want to extract information from the input image by using different filters.
Just like any (hidden) layer, these kinds of layers take an input, and then transform that input into an output. The transformation in a convolutional layer is done by a convolution operation. Mathematically speaking, the operations used is actually a cross-correlation, or a sliding dot-product. But for some reason the deep-learning community likes calling it a convolution. A convolution operation uses two sets of information and merges it into one. The first set of information is the input image itself, while the second is a feature detector, also called a filter (fig.\ref{fig:filter}).

\begin{figure}[h!]
\includegraphics[width=0.5\linewidth]{images/filter.png}
\centering
\caption{Illustration of the input image on the left, and the feature map/filter on the right.\cite{filter}}
\label{fig:filter}
\end{figure}
% https://towardsdatascience.com/applied-deep-learning-part-4-convolutional-neural-networks-584bc134c1e2

The convolution operation is performed by \textit{sliding} the filter over the input, and each time executing an element-wise matrix multiplication for all elements of the matrix, and summing those up. In figure \ref{fig:filter2}, we can see the first step of a convolution operation for the given image. Next, we will shift the filter one spot to the right, and compute the output for that product. We continue this until the filter has sled over the whole image, and the outputted feature map is fully computed. There can and will often be multiple filters, meaning this whole process will have to be repeated for each filter.

\begin{figure}[h!]
\includegraphics[width=0.5\linewidth]{images/filter2.png}
\centering
\caption{Illustration of a convolution operation for the first pixel. On the left, we see the superposition of the filter with the input. The output, on the right, is computed by making a pixel-wise multiplication and summing all the resulting values up.  \cite{filter}}
\label{fig:filter2}
\end{figure}

The actual values of the filters are learned by the neural network itself, although we will have to manually select the number of filters used and the filter size. The values for the filters will determine what kind of features are selected, the number of filters will influence the number of features selected, and the filter size will determine the size of the receptive field.

\subsection{Pooling}

To reduce overfitting and to shorten computing and training time, we often use pooling to reduce the size of the matrix. The pooling operation down-samples both in width and height, keeping the depth dimension intact. There are different types of pooling possible, but the most popular one is maxpooling. Like the convolution operation, it slides the window over the input. For each pooling window, it takes the maximum value and outputs it. A visualisation of a maxpooling operation can be seen in figure \ref{fig:maxpool}

\begin{figure}[h!]
\includegraphics[width=0.5\linewidth]{images/Maxpool.png}
\centering
\caption{In this illustration of a maxpool operation with window 2x2, we see that the operation consists in taking the maximum value for each window. Looking at the red square in the output, we see that the resulting value 20 is the highest value from all red squares in the input. We not that the dimensions of the matrix are halved.\cite{filter}}
\label{fig:maxpool}
\end{figure}


\subsection{CNN architecture}

Now that we have described all the basic building blocks of a convolutional network, let us explain the typical architecture of aforementioned, with figure \ref{fig:structCNN}. Starting with the input image, we perform a convolution with a certain number of filters. Secondly, we max-pool the output of the convolution, reducing the number of parameters to optimize. These first two steps can be repeated as much as desired, to increase the feature detection and its receptive field. Lastly, the result is flattened into a fully connected layer, and finally output into a class using an activation function. 

\begin{figure}[h!]
\includegraphics[width=0.75\linewidth]{images/structCNN.png}
\centering
\caption{Typical architecture of a CNN used for image classification\cite{CNNArch}}
\label{fig:structCNN}
\end{figure}


% CNNs use relatively little pre-processing compared to other image classification algorithms. This means that the network learns the filters that in traditional algorithms were hand-engineered. This independence from prior knowledge and human effort in feature design is a major advantage.


\subsection{Conclusion}

To summarise, artificial neural networks are a set of algorithms inspired by the human brain, that are constructed with as goal to cluster large data-sets or to classify data. They are made of a series of layers composed of neurons. The number of neurons can go from a few hundred to even millions of nodes that are very densely interconnected. Most of them of organised in a "feed-forward" way, meaning that each neuron gets a weighted input, only from precedent layers. To be able to classify data, a neural network has to be trained. This is done by minimising a loss function by adjusting the neurons weights as to fit the data as well as possible. 

CNN's are a variant of NN that are very popularly used in image processing. By means of convolution operations, pooling layers and fully connected layers, they are designed to automatically learn spatial hierarchies of features. Which is why they are such a good fit for image processing, and for our pollen classification problem.























% \begin{figure}[h!]
% \centering
% \begin{subfigure}{0.5\textwidth}
%  \includegraphics[width=0.45\linewidth]{images/NNex2.png}
%  \centering
%  \caption{Illustration of the different types of layers in a NN}
%  \label{fig:NNex2}
% \end{subfigure}%

% \begin{subfigure}{0.5\textwidth}
%   \includegraphics[width=0.45\linewidth]{images/GradDes.png}
%   \centering
%   \caption{Illustration of the gradient descent method}
%   \label{fig:GradDes}
% \end{subfigure}

% \end{figure}





% \begin{example}
% Consider the partially sampled matrix $\matr{M}$ defined as
% \begin{equation*}
%   \matr{M} =
%   \begin{bmatrix}
%     1 & 2 & 3  \\
%     2 & 4 & \star  \\
%     \star & \star & 9
% \end{bmatrix},
% \end{equation*}
% where each unsampled entry is denoted as $\star$.
% We have $\Omega = \left\{\parent{1, 1}; \parent{1, 2}; \parent{1, 3}; \parent{2, 1}; \parent{2, 2}; \parent{3, 3}\right\}$.
% There are an infinite number of completions of $\matr{M}$.
% Let us give two examples, namely the matrices
% \begin{equation*}
%   \matr{X}_{1} =
%   \begin{bmatrix}
%     1 & 2 & 3  \\
%     2 & 4 & {\color{blue} 6}  \\
%     {\color{blue} 3} & {\color{blue} 6} & 9
% \end{bmatrix},
%  \quad
%   \matr{X}_{2} =
%   \begin{bmatrix}
%     1 & 2 & 3  \\
%     2 & 4 & {\color{blue} 6}  \\
%     {\color{blue} 3} & {\color{blue} 7} & 9
% \end{bmatrix}.
% \end{equation*}


% We see that $\rank \parent{\matr{X}_{1}} = 1$, while $\rank \parent{\matr{X}_{2}} = 2$.
% Hence, if the rank $r$ is fixed to $1$ for this problem, then $\matr{X}_{1}$ is a solution to the LRMC problem, while $\matr{X}_{2}$ is not.
% %One can also think about other situations where the LRMC problem does not have a unique solution.  
% Of course, this is only a small example: in real applications the matrices that we are trying to fill are much larger.
% \end{example}

