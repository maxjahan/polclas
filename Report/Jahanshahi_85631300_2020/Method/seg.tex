
% Decrire les difficultés rencontrées?
% 


In this chapter, the core method investigated in this master's thesis will be explained. First, Section \ref{sec:Unet} will go over the choice of architecture for the CNN, explaining in detail why this specific structure was chosen and how it works. Then, in section \ref{sec:seg} and \ref{sec:clas} we will explain how we went from a theoretic model, to a practical, working, tool. 
Section \ref{sec:seg} will describe how we created the pollen segmentator, a tool that is able to recognise pollen grain pixels from background pixels. Then, section \ref{sec:clas} will get at the heart of the matter, explaining how we created a neural network able to distinguish, at a pixel level, a pollen species from another. In both of these last sections, we will explain the important implementation choices we made while constructing the networks.


\section{The U-net architecture} \label{sec:Unet}

The U-net is a convolutional network, specifically designed for biological image segmentation. It was imagined and developed in 2015 by Olaf Ronneberger, Phillip Fischer, and Thomas Brox at the University of Freiburg, Germany \cite{ronnerB}. As of today, it is considered as one of the standard and most efficient CNN architectures for image segmentation, i.e. pixel-wise labeling. \cite{UnetExpl}. 

First and foremost, why is a U-net architecture a good choice for segmenting and classifying pollen grains? What are the advantages and drawbacks of such an architecture compared to a classic CNN, composed of a sequence of layers at the same resolution than the native image? 

In the first place, in deep learning, it is widely known that we often need huge amounts of data to make a model robust. And sometimes, we can not afford that. Like in this case, collecting and annotating multiple thousand of pollen grains per species is not doable. Fortunately, this architecture accounts for exactly that. U-net is very effective even with a limited data-set. The use of lots of data augmentation on the available training data, ensures that the networks learn invariance to such deformations\cite{Heartbeat}.

Firstly, U-net architecture, by considering multiple scales, allows to combines localised information with contextual information.
Note that, since the prediction is a pixel-wise class label, sub-sampled features have to be upsampled to feed the network output. This leads to a hourglass architecture, also named encoder-decoder architecture.

Secondly, the U-Net involves skip connections, to directly transfer the high-resolution features that are computed from the input to the layer of same resolution in the upsampling path leading to the network output. Those skip connections help the network to merge local and contextual information, but also favors the back-propagation of gradients.  \cite{MediumUnet}.



\begin{figure}[t]
\includegraphics[width=1.1\linewidth]{images/U-net.png}
\centering
\caption{U-net architecture. The blue boxes represent  multi-channel feature maps. The number of channels is denoted on top of the box. The size of the array is denoted in the left lower corner of the image. The white boxes represent copied feature maps. The arrows denote the different operations \cite{ronnerB}}
\label{fig:Unet}
\end{figure}

A general outline of a U-net can be seen in figure \ref{fig:Unet}. At first glance from the sketch, we can understand that the \textit{"U"} in U-net comes from it shape. In the structure of the network, we can discern two major pathways. The first one being the contraction path, which is composed of classic convolutions and max pools. The second path is the expansion path, composed of transposed convolutions. It is used for up-sampling and enables to possibility of getting an output with the same dimensions as the input, granting precise localisation to the CNN.


\subsection{Contraction path}

The contraction, or down sampling path, is composed of a repetition of convolutions followed by a ReLU and a max pooling operation. On the figure above, we see that the convolutions are 3x3 and unpadded(meaning that the border pixels are lost). We can see on the illustration that after each layer, the number of features(indicated on top of the blue squares) is doubled. This down sampling also increases the complexity of the features. In conclusion, this path is used to capture the context with the help of a compact feature map\cite{UnetExpl}.


\subsection{Expansion path}

The up-sampling path or expansion path, consists of the up-sampling of the feature map via three major steps that are repeated.  Firstly, an up-convolution(transposed convolutions), which expands the size of the image. The second operation is a concatenation of the output from the last operation with the corresponding image from the contraction path. The third operation is 2 regular convolutions. The second operation combines localised, and contextual information. This is what makes the U-net so special.

% \subsection{Conclusion}

\section{Universal pollen segmentator } \label{sec:seg}

This section describes the methodology used to implement the U-net algorithm we reported in the previous section to create a universal pollen segmentation tool. This tool labels background pixels with zero, and pollen pixel with one, whatever the pollen species.

The first step to differentiate pollen grains from each other, is being able to discern them from everything else. Pollen grains are often visually very similar, and we can exploit this feature and make it an advantage in our search for a powerful segmentator. 
This tool will be assigning either the pollen label to a pixel, or in the other case, the background label. 
Thus, we are trying to develop a tool capable of binary pixel segmentation.  

% Code part
We programmed the entirety of the U-net network in \texttt{Python} \cite{Python}, a very popular, high-level programming language. The model was implemented with the help of Keras \cite{Keras}, an open-source neural network library. Keras facilitates the design and implementation of NN by being user-friendly, modular and highly extensible. A more detailed choice of this particular library can be found in appendix \ref{appendix:Impl}.
In the following paragraphs, we will go over some important implementation choices we took for the parameters of our segmentation algorithm.


% Activation f
In machine learning, when developing a CNN, we have to choose activation functions corresponding with the output we want. 
In this case, we want to predict a binary outcome : pollen or background.
A good choice for an activation function for this kind of prediction would be a sigmoid. 
As a reminder, a sigmoid will assign for all pixels a value between 0 and 1 for each class, depending on how confident the model is with the pixel being in the class.
From that result, the algorithm will just choose the highest value, and that will be the predicted class for that pixel.

Another primordial choice for the construction of the segmentator is the choice of the loss function. 
This is the function whose minimisation will lead the CNN to predict what is expected. The gradient descent optimization algorithm used to train the network will be based on the gradient of the loss function with respect to the network parameters. In practice, those gradients are computed using the chain rule, through the back-propagation algorithm.\cite{DeepLearning}
We chose to use the Binary Cross Entropy function. The mathematical equation for the cross-entropy is depicted in figure \ref{Eq1}.
It's powerful and efficient, cross entropy loss will measure the performance of a classifier whose output is a probability between zero and one. The cross entropy loss will rise increase as the predictions for the labels are different. Because there is a logarithm in the function, the loss increases very rapidly when the predicted probability is high. This results in the harsh penalisation of confident predictions that are not correct.



\begin{equation}
    - \sum_{c=1}^My_{o,c}\log(p_{o,c})
    \label{Eq1}
\end{equation} 

In this equation: 

\begin{itemize}
    \item The M denotes the number of classes
    \item y is a binary indicator (0 or 1) if class label c is the correct classification for observation o
    \item p is the predicted probability that observation o is of class c
\end{itemize}


Finally, a last important hyper-parameter is the number of layers in the network. The number of layers in a image-processing CNN is actually an important factor. It influences the size of its receptive field. Remember that the receptive field in a CNN is the region of the input space that affects a particular node of the network. A neuron will thus be able to detect a feature such as an edge or a line, if the object is smaller than the receptive field. 

When working with a U-net architecture, the size of the receptive field will double with every layer due to the max-pooling operations. Additionally, the bottleneck(deepest part in the U-net) also doubles the receptive fields .
When inspecting various different species of pollen, we observed that the largest pollen grains have a maximum of around 30 pixels. We can now calculate the minimum amount of layers needed to be able to detect the grains: Taking 4 layers, we arrive at a receptive field of $2^{4+1} = 32 $ pixels(The "+1" is for the bottleneck). To be safe but not taking an unreasonable and excessive amount of layers, we chose to select a number of 5 total layers. 


Now we have designed and implemented an instrument capable of discerning pollen grains from background or other impurities in an image, we are one step away from our final goal.
Thanks to this tool, we can now very easily identify pollen pixels in new data. Keep in mind that the quality of the segmentator will depend on the amount of data is supplied to the training algorithm. Thanks to the similar nature of pollen grains, and assuming that we supplied the segmentator with a substantial amount of data, we can now identify and annotate any pollen type. 



