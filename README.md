In this repository, you will find my Master thesis

Title: CNN for segmentation and classification of pollen grains

Supervisors:  Christophe De Vleeschouwer

Readers: Victor Joos de ter Beerst, Anne-Laure Jacquemart

You can find the Latex code for compiling the report in the "report" section. The "Data" directory contains the data-set downloaded from Cytomine.
The "Code" section contains the python code for the construction of a Classification CNN model.